# Pandas example

This is a minimal repo to upload a pandas example to gitlab for a job ad.
The example is a simple clustering gmm analysis applied to trading data to check the liquidity of markets.

The example is a google colab notebook.
You can open it in google colab or locally in VS Code or other suitable editors.


I will include the data source csv for completeness.
This will be deleted afterwards, since there is no point otherwise for it to be public.

----------------------
Steffen Roßkopf 